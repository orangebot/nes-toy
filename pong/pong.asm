	.inesprg 1	; 1x 16KB PRG code
	.ineschr 1	; 1x 8KB CHR data
	.inesmap 0	; mapper 0 = NROM, no bank swapping
	.inesmir 1	; background mirroring


;;;;;;;;;;;;;;;


	.rsset $0000	; Set some global variables


;;;;;;;;;;;;;;;


	.bank 0
	.org $C000

vblankwait:		; wait for vblank to make sure PPU is ready
	BIT $2002
	BPL vblankwait
	RST

RESET:
	SEI				; disable IRQs
	CLD				; disable decimal mode
	LDX #$40
	STX $4017		; disable APU frame IRQ
	LDX #$FF
	TXS				; Set up stack
	INX				; now X = 0
	STX $2000		; disable NMI
	STX $2001		; disable rendering
	STX $4010		; disable DMC IRQs

	JSR vblankwait	; First wait for vblank

clrmem:
	LDA #$00
	STA $0000, x
	STA $0100, x
	STA $0200, x
	STA $0400, x
	STA $0500, x
	STA $0600, x
	STA $0700, x
	LDA #$FE
	STA $0300, x
	INX
	BNE clrmem

	JSR vblankwait	; Second wait for vblank

	LDA #%00000000	; black background
	STA $2001


LoadPalettes:
	LDA $2002		; read PPU status to reset the high/low latch
	LDA #$3F
	STA $2006		; write the high byte of $3F00 address
	LDA #$00
	STA $2006		; write the low byte of $3F00 address
	LDX #$00		; start out at 0
LoadPalettesLoop:
	LDA palette, x	; load data from address (palette + the value in x)
					; 1st time through loop it will load palette+0
					; 2nd time through loop it will load palette+1
					; 3rd time through loop it will load palette+2
					; etc
	STA $2007		; write to PPU
	INX				; X = X + 1
	CPX #$20		; Compare X to hex $10, decimal 16 - copying 16 bytes = 4 sprites
	BNE LoadPalettesLoop	; Branch to LoadPalettesLoop if compare was Not Equal to zero
					; if compare was equal to 32, keep going down


Forever:
	JMP Forever		;jump back to Forever, infinite loop



NMI:
	RTI

;;;;;;;;;;;;;;	



	.bank 1

	.org $E000
palette:
	; background palette
	.db $22,$29,$1A,$0F, $22,$36,$17,$0F, $22,$30,$21,$0F, $22,$27,$17,$0F
	; sprite palette
	.db $22,$1C,$15,$14, $22,$02,$38,$3C, $22,$1C,$15,$14, $22,$02,$38,$3C

	.org $FFFA		;first of the three vectors starts here
	.dw NMI			;when an NMI happens (once per frame if enabled) the 
					;processor will jump to the label NMI:
	.dw RESET		;when the processor first turns on or is reset, it will jump
					;to the label RESET:
	.dw 0			;external interrupt IRQ is not used in this tutorial


;;;;;;;;;;;;;;	


	.bank 2
	.org $0000
	.incbin "mario.chr"		;includes 8KB graphics file from SMB1
